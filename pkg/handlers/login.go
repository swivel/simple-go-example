package handlers

import (
	"net/http"

	"my-calc/pkg/models"

	"github.com/gin-gonic/gin"
)

func PostLogin(c *gin.Context) {
	var userInst models.UserModel
	c.Bind(&userInst)
	c.JSON(http.StatusOK, userInst)
}

