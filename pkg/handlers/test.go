package handlers

import (
	"net/http"

	"my-calc/pkg/models"

	"github.com/gin-gonic/gin"
)

func GetTest(c *gin.Context) {
	testInst := &models.TestModel{
		Foobar: "Hello World",
	}
	c.JSON(http.StatusOK, testInst)
}

func PostTest(c *gin.Context) {
	var testInst models.TestModel
	c.Bind(&testInst)
	c.JSON(http.StatusOK, testInst)
}
