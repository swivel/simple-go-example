/*
Copyright © 2022 Joseph Dalrymple me@swivel.in

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
This file heavily influenced by the magical work by carolynvs:
https://github.com/carolynvs/stingoftheviper/blob/main/main.go
*/
package cmd

import (
	"fmt"
	"os"
	"strings"
	"path/filepath"

	"my-calc/pkg/routes"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/spf13/pflag"
)

const (
	configDir = "my-calc"
	configFile = "config"
	configType = "yaml"
	envPrefix = "MY_CALC"
)

var cfgFile string

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cmd := RootCommand()
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func RootCommand() *cobra.Command {
	toggle := false

	// rootCmd represents the base command when called without any subcommands
	var rootCmd = &cobra.Command{
		Use:   "my-calc",
		Short: "A brief description of your application",
		Long: "A longer description that spans multiple lines and likely contains\n" +
				"examples and usage of using your application. For example:\n\n" +
				"Cobra is a CLI library for Go that empowers applications. This\n"+
				"application is a tool to generate the needed files to quickly\n"+
				"create a Cobra application.",
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			// You can bind cobra and viper in a few locations, but PersistencePreRunE on the root command works well
			return initConfig(cmd)
		},
		// Uncomment the following line if your bare application
		// has an action associated with it:
		Run: func(cmd *cobra.Command, args []string) {
			tgl := cmd.Flag("toggle")
			fmt.Println("command called")
			fmt.Fprintln(os.Stdout, "Value of Toggle:", toggle, "or", tgl.Value)

			r := routes.NewRouter()
			r.Run()
		},
	}

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/my-calc/config.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP(&toggle, "toggle", "t", false, "Help message for toggle")
	rootCmd.Flags().BoolVar(&toggle, "toggle", false, "Help message for toggle")

	return rootCmd
}

// initConfig reads in config file and ENV variables if set.
func initConfig(cmd *cobra.Command) error {
	v := viper.New()

	if cfgFile != "" {
		// Use config file from the flag.
		v.SetConfigFile(cfgFile)
	} else {
		// Find config directory.
		userCfg, err := os.UserConfigDir()
		cobra.CheckErr(err)

		v.AddConfigPath(".")

		// Search in user config directory for config with name ".my-calc" (without extension).
		v.AddConfigPath(filepath.Join(userCfg, configDir))
		v.SetConfigType(configType)
		v.SetConfigName(configFile)
	}

	// If a config file is found, read it in.
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return err
		}
	} else {
		fmt.Fprintln(os.Stderr, "Using config file:", v.ConfigFileUsed())
	}

	v.SetEnvPrefix(envPrefix)
	v.AutomaticEnv() // read in environment variables that match

	bindFlags(cmd, v)

	return nil
}

func bindFlags(cmd *cobra.Command, v *viper.Viper) {
	cmd.Flags().VisitAll(func(f *pflag.Flag) {
		if strings.Contains(f.Name, "-") {
			envVarSuffix := strings.ToUpper(strings.ReplaceAll(f.Name, "-", "_"))
			v.BindEnv(f.Name, fmt.Sprintf("%s_%s", envPrefix, envVarSuffix))
		}

		if !f.Changed && v.IsSet(f.Name) {
			val := v.Get(f.Name)
			cmd.Flags().Set(f.Name, fmt.Sprintf("%v", val))
		}
	})
}
